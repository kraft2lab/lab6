#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <assert.h>

#include "stdlib.h"
#include "string.h"

/*
Adam Kraft
kraft2
Lab 5
Lab Section: 5
Hanjie Liu
*/

using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

class DeckOfCards {
public:

  //initialize members
  DeckOfCards() : hand(new Card[5]){
    initDeck();  
  }
  //free resources
  ~DeckOfCards() {
    delete[] hand;
  }

  void shuffleDeck() {
    random_shuffle(&deck[0], &deck[52], myrandom);
  }

  Card* getHandFromDeck() {
    for (int i = 0; i < 5; i++) {
      hand[i] = deck[i];
    }
    return hand;
  }

  //wrapper for sort function
  void sortHand() {
    sortCardArr(&hand[0], &hand[5]);
  }

private:
  void initDeck() {
    //deck should have 13 of each suit
    //4 suits
    //52 cards
    int count = 0;  
    for (int s = 0; s < 4; s++) {
      Suit currentSuit = getSuit(s);		  
      for(int i = 2; i < 15; i++) {
        Card currentCard;
        currentCard.suit = currentSuit;
        currentCard.value = i;
        deck[count] = currentCard;
        count++;
      }
    }
  }

  Card deck[52];
  Card* hand;

  Suit getSuit(int suitNum) {
    //there are only four suits
    assert(suitNum >= 0 && suitNum < 5 && 
    "There are only four suits. lab5.cpp - line82");

    switch(suitNum) {
      case 0:
        return SPADES;
      case 1:
        return HEARTS;
      case 2:
        return DIAMONDS;
      case 3: 
        return CLUBS; 
      default:
        assert(false && "Control should not reach here. lab5.cpp - line94");
        return SPADES;             
    }
  } 

  void sortCardArr(Card* begin, Card* end) {
	//sort the given array  
    sort(begin, end, suit_order);
  }

}; //end of DeckOfCards class


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
  *initialize the deck*/
  DeckOfCards cardDeck;

  /*After the deck is created and initialzed we call random_shuffle() see the
  *notes to determine the parameters to pass in.*/
  cardDeck.shuffleDeck();

  /*Build a hand of 5 cards from the first five cards of the deck created
  *above*/
  Card* myHand = cardDeck.getHandFromDeck();

  /*Sort the cards.  Links to how to call this function is in the specs
  *provided*/
  cardDeck.sortHand();

  /*Now print the hand below. You will use the functions get_card_name and
  *get_suit_code */
  for (int i = 0; i < 5; i++) {
    cout << setw(10) << right;
    cout << get_card_name(myHand[i]);
    cout << get_suit_code(myHand[i]) << endl;		
  }
  
  return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit == rhs.suit) {
    return lhs.value < rhs.value; 
  }
  return lhs.suit < rhs.suit;
}

string get_suit_code(Card& c) {	
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  string cardName;
  //get string for non face cards
  if (c.value < 11) {
    ostringstream cardNum; 
    cardNum << c.value; 
    cardName = cardNum.str();
  }
  else {
	//differentiate between face cards  
    switch (c.value) {
      case 11:
        cardName = "Jack"; 
      break;
      case 12:
        cardName = "Queen";
      break;
      case 13:
        cardName = "King";
      break;
      case 14: 
        cardName = "Ace";
      break;
      default:
        assert(false && "Control should never reach here. lab5.cpp - line 182");
      break;
    }
  }
  return cardName += " of ";
}
